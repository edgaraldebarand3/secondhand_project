"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.changeColumn("Profiles", "phone_number", {
      type: Sequelize.STRING,
    });
  },

  async down(queryInterface, Sequelize) {
    queryInterface.changeColumn("Profiles", "phone_number", {
      type: Sequelize.INTEGER,
    });
  },
};

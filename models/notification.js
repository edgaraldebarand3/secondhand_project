"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class notification extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      notification.belongsTo(models.Transaction, {
        foreignKey: "transaction_id",
      });
      notification.belongsTo(models.Product, {
        foreignKey: "product_id",
      });
      notification.belongsTo(models.User, {
        foreignKey: "user_id",
      });
    }
  }
  notification.init(
    {
      user_id: DataTypes.INTEGER,
      product_id: DataTypes.INTEGER,
      message: DataTypes.STRING,
      status: DataTypes.ENUM({
        values: ["Penawaran produk", "Produk diterbitkan"],
      }),
      createdAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "notification",
    }
  );
  return notification;
};

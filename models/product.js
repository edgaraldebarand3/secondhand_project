"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Product.hasMany(models.Transaction, {
        foreignKey: "product_id",
      });
      Product.hasMany(models.notification, {
        foreignKey: "product_id",
      });
      Product.belongsTo(models.Category, {
        foreignKey: "category_id",
      });
      Product.belongsTo(models.User, {
        foreignKey: "user_id",
      });
      Product.hasMany(models.notification, {
        foreignKey: "product_id",
      });
    }
  }
  Product.init(
    {
      name: DataTypes.STRING,
      price: DataTypes.INTEGER,
      description: DataTypes.STRING,
      product_picture_url: DataTypes.ARRAY(DataTypes.STRING),
      product_picture_url_publicid: DataTypes.ARRAY(DataTypes.STRING),
      quantity: DataTypes.INTEGER,
      sold: DataTypes.BOOLEAN,
      is_publish: DataTypes.BOOLEAN,
      user_id: DataTypes.INTEGER,
      category_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Product",
    }
  );
  return Product;
};

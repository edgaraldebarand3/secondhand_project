"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Profile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Profile.belongsTo(models.City, {
        foreignKey: "city_id",
      });
      Profile.belongsTo(models.User, {
        foreignKey: "user_id",
      });
    }
  }
  Profile.init(
    {
      name: DataTypes.STRING,
      address: DataTypes.STRING,
      phone_number: DataTypes.INTEGER,
      profile_picture_url: DataTypes.STRING,
      profile_picture_url_publicid: DataTypes.STRING,
      city_id: DataTypes.INTEGER,
      user_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Profile",
    }
  );
  return Profile;
};

"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Transaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Transaction.hasMany(models.notification, {
        foreignKey: "transaction_id",
      });
      Transaction.belongsTo(models.User, {
        foreignKey: "buyer_id",
      });
      Transaction.belongsTo(models.User, {
        foreignKey: "seller_id",
      });
      Transaction.belongsTo(models.Product, {
        foreignKey: "product_id",
      });
    }
  }
  Transaction.init(
    {
      bargain_price: DataTypes.INTEGER,
      confirmation: DataTypes.BOOLEAN,
      buyer_id: DataTypes.INTEGER,
      seller_id: DataTypes.INTEGER,
      product_id: DataTypes.INTEGER,
      deletedAt: DataTypes.DATE,
    },
    {
      sequelize,
      paranoid: true,
      modelName: "Transaction",
    }
  );
  return Transaction;
};

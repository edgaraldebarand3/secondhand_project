"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcrypt");
const { Profile } = require("./profile");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.Product, {
        foreignKey: "user_id",
      });
      User.hasMany(models.Transaction, {
        foreignKey: "buyer_id",
      });
      User.hasMany(models.notification, {
        foreignKey: "user_id",
      });
      User.hasMany(models.Transaction, {
        foreignKey: "seller_id",
      });
      User.hasOne(models.Profile, {
        foreignKey: "user_id",
      });
    }
  }
  User.init(
    {
      username: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      otp: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "User",
      hooks: {
        beforeCreate: async (user, options) => {
          user.password = bcrypt.hashSync(
            user.password,
            +process.env.SALT_ROUNDS
          );
          return user;
        },
      },
    }
  );
  return User;
};

const router = require("express").Router();
const {
  postMyProduct,
  publishMyProduct,
  putMyProduct,
  confirmTransaction,
  changeTransactionStatus,
  getAllMyProduct,
  getMyProductById,
  getAllMySoldProduct,
  getMySoldProductById,
  getAllMyTransaction,
  getMyTransactionById,
  getMyTransactionByIdConfirmationNull,
  getMyTransactionByIdSold,
  deleteMyProduct,
} = require("../controllers/seller.controllers");
const { verifyToken } = require("../misc/auth");
const multerProduct = require("../handler/multerProduct");

// Seller CRUD

router.post(
  "/upload",
  verifyToken,
  multerProduct.array("product_picture_url", 5),
  postMyProduct
);
router.put(
  "/myproduct/publish/:id",
  verifyToken,
  multerProduct.array("product_picture_url", 5),
  publishMyProduct
);
router.put(
  "/myproduct/:id",
  verifyToken,
  multerProduct.array("product_picture_url", 5),
  putMyProduct
);
router.get("/myproduct", verifyToken, getAllMyProduct);
router.get("/myproduct/sold", verifyToken, getAllMySoldProduct);
router.get("/mytransaction", verifyToken, getAllMyTransaction);
router.put("/myproduct/confirm/:id", verifyToken, confirmTransaction);
router.put("/myproduct/status/:id", verifyToken, changeTransactionStatus);
router.get("/myproduct/:id", verifyToken, getMyProductById);
router.get("/myproduct/sold/:id", verifyToken, getMySoldProductById);
router.get("/mytransaction/:id", verifyToken, getMyTransactionById);
router.delete("/myproduct/:id", verifyToken, deleteMyProduct);

module.exports = router;

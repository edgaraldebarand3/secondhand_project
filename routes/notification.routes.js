const router = require("express").Router();
const notificationController = require("../controllers/notifications.controllers");
const { verifyToken } = require("../misc/auth");

router.get("/", verifyToken, notificationController.getNotif);

module.exports = router;

const router = require("express").Router();
const transactionControllers = require("../controllers/transaction.controllers");
const { verifyToken } = require("../misc/auth");

//Get Semua Transaksi
router.get("/", verifyToken, transactionControllers.getAllTransaction);

//Get Transaksi By Id
router.get(
  "/transaction/byproductid/:id",
  verifyToken,
  transactionControllers.getTransactionByProductId
);
router.post("/", verifyToken, transactionControllers.createTransaction);

module.exports = router;

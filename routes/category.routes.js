const router = require("express").Router();
const categoryController = require("../controllers/category.controllers");

router.get("/", categoryController.getAllCategory);
router.get("/:id", categoryController.getCategoryById);

module.exports = router;

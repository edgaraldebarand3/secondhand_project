const router = require("express").Router();
const userController = require("../controllers/users.controllers");
const {
  getOTP,
  resetPassword,
} = require("../controllers/forgotPass.controllers");
const validate = require("../validation/validate");
const { verifyToken } = require("../misc/auth");
const userValidation = require("../validation/user.validation");

//User login & register
router.post("/login", userController.login);
router.post(
  "/register",
  userValidation.register(),
  validate,
  userController.register
);

//User CRUD
router.get("/", userController.getAll);
router.put("/:id", userController.update);
// Change Password
router.post(`/password/reset?`, resetPassword);

// Get Reset Link to Email
router.post("/forgot-password", getOTP);

module.exports = router;

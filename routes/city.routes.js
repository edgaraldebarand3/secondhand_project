const router = require("express").Router();
const cityController = require("../controllers/city.controllers");

router.get("/", cityController.getAllCity);
router.get("/:id", cityController.getCityById);
router.post("/", cityController.createCity);
router.delete("/", cityController.deleteCity);

module.exports = router;

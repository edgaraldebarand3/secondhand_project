const express = require("express");
const router = express.Router();
// const { logger, sanitation } = require("../misc/middleware");
const userRoutes = require("./users.routes");
const profileRoutes = require("./profile.routes");
const sellerRoutes = require("./seller.routes");
const buyerRoutes = require("./buyer.routes");
const cityRoutes = require("./city.routes");
const categoryRoutes = require("./category.routes");
const notificationRoutes = require("./notification.routes");
const transactionRoutes = require("./transaction.routes");

router.use("/user", userRoutes);
router.use("/profile", profileRoutes);
router.use("/seller", sellerRoutes);
router.use("/buyer", buyerRoutes);
router.use("/city", cityRoutes);
router.use("/category", categoryRoutes);
router.use("/notification", notificationRoutes);
router.use("/transaction", transactionRoutes);

module.exports = router;

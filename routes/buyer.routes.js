const router = require("express").Router();
const buyerController = require("../controllers/buyer.controllers");
const { verifyToken, verifyTokenProductBuyer } = require("../misc/auth");

router.get("/", verifyTokenProductBuyer, buyerController.getAllProduct);
router.get("/:id", verifyToken, buyerController.getProductById);

module.exports = router;

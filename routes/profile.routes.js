const router = require("express").Router();
const profileController = require("../controllers/profile.controllers");
const { verifyToken } = require("../misc/auth");
const uploadImage = require("../handler/multerImage");
const { uploadImageWithCloudinary } = require("../handler/cloudinary");

router.put(
  "/",
  verifyToken,
  uploadImage.single("profile_picture_url"),
  profileController.update
);
router.get("/", verifyToken, profileController.getProfile);

module.exports = router;

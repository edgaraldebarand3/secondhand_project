"use strict";
const profileData = require("../masterdata/profile.json");
const profileDataMapped = profileData.map((eachProfileData) => {
  eachProfileData.createdAt = new Date();
  eachProfileData.updatedAt = new Date();
  return eachProfileData;
});
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert("Profiles", profileDataMapped, {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Profiles", null, {
      truncate: true,
      restartIdentity: true,
    });
  },
};

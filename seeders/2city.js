"use strict";
const cities = require("../masterdata/cities.json");
const cityDataMapped = cities.map((eachCityData) => {
  eachCityData.createdAt = new Date();
  eachCityData.updatedAt = new Date();
  return eachCityData;
});
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert("Cities", cityDataMapped, {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Cities", null, {
      truncate: true,
      restartIdentity: true,
    });
  },
};

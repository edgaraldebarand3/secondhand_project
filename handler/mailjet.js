const mailjet = require("node-mailjet").apiConnect(
  `a065e45c2d73e890ec8b6573e9c3898b`,
  `87398846b76fdff915fe9fe8ccdf991a`
);

const registerMail = (email, username) => {
  const reciever = email;
  const name = username;
  mailjet.post("send", { version: "v3.1" }).request({
    Messages: [
      {
        From: {
          Email: "dicky2ux@gmail.com",
          Name: "SecondHand",
        },
        To: [
          {
            Email: `${reciever}`,
            Name: `${name}`,
          },
        ],
        Subject: "Welcome to SecondHand",
        TextPart: "Registering Email",
        HTMLPart:
          "<h3>Thanks for creating an Account, welcome to <a href='https://secondhand-fe3.vercel.app/'>SecondHand</a>!</h3><br />May the SecondHand force be with you!",
        CustomID: "AppGettingStartedTest",
      },
    ],
  });
};

const sendMail = (email, username, hashedOTP) => {
  const reciever = email;
  const name = username;
  const otp = hashedOTP;
  mailjet.post("send", { version: "v3.1" }).request({
    Messages: [
      {
        From: {
          Email: "dicky2ux@gmail.com",
          Name: "SecondHand",
        },
        To: [
          {
            Email: `${reciever}`,
            Name: `${name}`,
          },
        ],
        Subject: "Reset Password",
        TextPart: "Reset Password",
        HTMLPart: `<h3>Click the link below to reset your password</h3><br /><a href='https://secondhand-fe3.vercel.app/user/password/reset?reset=${otp}'>Reset Password</a>`,
        CustomID: "AppGettingStartedTest",
      },
    ],
  });
};

module.exports = { registerMail, sendMail };

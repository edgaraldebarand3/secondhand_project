const {
  Transaction,
  Product,
  Category,
  notification,
  User,
  Profile,
  City,
} = require("../models");
const { Op } = require("sequelize");
const { verifyToken } = require("../misc/auth");

const buyerController = {
  getAllProduct: async (req, res) => {
    try {
      let { name, category_id, page, row } = req.query;
      page -= 1;
      let options = {
        where: {
          user_id: { [Op.not]: req.user.id },
          is_publish: true,
          sold: false,
        },
        attributes: {
          exclude: [
            "createdAt",
            "updatedAt",
            "is_publish",
            "product_picture_url_publicid",
          ],
        },
        offset: page * row,
        limit: row,
        order: [["name", "ASC"]],
      };
      if (name) {
        options.where.name = {
          [Op.iLike]: `%${name}%`,
        };
      }
      if (category_id) {
        options.where.category_id = category_id;
      }
      const AllProduct = await Product.findAll(options);

      // for (let product of AllProduct) {
      //   let result
      // }

      return res.status(201).json({
        code: 201,
        status: "Sucess",
        result: AllProduct,
      });
    } catch (error) {
      console.log(error);
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: "System Error",
      });
    }
  },
  getProductById: async (req, res) => {
    const productId = req.params.id;
    try {
      const options = {
        attributes: [
          "id",
          "name",
          "price",
          "product_picture_url",
          "description",
          "user_id",
          "sold",
        ],
        include: [
          {
            model: User,
            attributes: ["email"],
            include: {
              model: Profile,
              attributes: ["name", "profile_picture_url"],
              include: {
                model: City,
                attributes: ["name"],
              },
            },
          },
          {
            model: Category,
            attributes: ["category"],
          },
        ],
      };
      const foundProduct = await Product.findByPk(productId, options);

      //If User Choose His Own Product
      if (foundProduct.user_id === req.user.id) {
        return res.status(400).json({
          code: 400,
          status: "Failed",
          message: "Can't Get Product",
        });
      }

      const foundTransaction = await Transaction.findOne({
        where: {
          buyer_id: req.user.id,
          seller_id: foundProduct.user_id,
          product_id: foundProduct.id,
        },
      });

      if (foundTransaction) {
        if (foundTransaction.confirmation == null) {
          return res.status(200).json({
            code: 200,
            status: "Sucess",
            message: "Menunggu Respon Penjual",
            result: foundProduct,
          });
        } else if (foundTransaction.confirmation == true) {
          return res.status(200).json({
            code: 200,
            status: "Sucess",
            message: "Penawaran Di Terima",
            result: foundProduct,
          });
        } else if (
          foundTransaction.confirmation == true &&
          foundProduct.sold == true
        ) {
          return res.status(200).json({
            code: 200,
            status: "Sucess",
            message: "Product Berhasil Terbeli",
            result: foundProduct,
          });
        }
      }
      //Sucess
      return res.status(200).json({
        code: 200,
        status: "Sucess",
        result: foundProduct,
      });
    } catch (error) {
      console.log(error);
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: "System Error",
      });
    }
  },
};

module.exports = buyerController;

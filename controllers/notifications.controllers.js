const {
  notification,
  User,
  Category,
  Product,
  Transaction,
  sequelize,
} = require("../models");
const { Op } = require("sequelize");
const moment = require("moment");

const notificationController = {
  getNotif: async (req, res) => {
    try {
      const options = {
        attributes: [
          "id",
          "transaction_id",
          "product_id",
          "message",
          "status",
          [
            sequelize.fn(
              "to_char",
              sequelize.col("notification.createdAt"),
              "DD Mon, HH24:MI:SS"
            ),
            "createdAt",
          ],
        ],
        where: {
          transaction_id: { [Op.not]: null },
        },
        include: [
          {
            model: Transaction,
            attributes: ["id", "bargain_price"],
            where: {
              id: { [Op.not]: null },
            },
          },
          {
            model: Product,
            attributes: [
              "name",
              "price",
              "product_picture_url",
              "description",
              "category_id",
            ],
            include: {
              model: Category,
              attributes: ["category"],
            },
          },
        ],
        where: {
          user_id: req.user.id,
        },
        order: [["id", "DESC"]],
      };

      const findAllNotification = await notification.findAll(options);
      return res.status(200).json({
        code: 200,
        status: "Sucess",
        data: findAllNotification,
      });
    } catch (error) {
      console.log(error);
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: "System Error",
      });
    }
  },
};

module.exports = notificationController;

const {
  User,
  Profile,
  Product,
  Transaction,
  notification,
  Category,
  City,
  sequelize,
} = require("../models");
const {
  cloudinaryImageUploadMethod,
  uploadImageWithCloudinary,
  cloudinaryImageDeleteMethod,
} = require("../handler/cloudinary");

const postMyProduct = async (req, res) => {
  try {
    const { is_publish } = req.query;
    if (is_publish === "false") {
      const urls = [];
      const files = req.files;

      if (files.length <= 0) {
        return res.status(403).json({
          code: 403,
          status: "Failed",
          message: "Foto Produk Harus Di Isi",
        });
      }
      //upload image to cloudinary
      for (const file of files) {
        const { path } = file;
        const newPath = await cloudinaryImageUploadMethod(path);
        urls.push(newPath);
      }

      const { name, price, description, quantity, category_id } = req.body;
      const previewProduct = await Product.create({
        name: name,
        price: price,
        description: description,
        product_picture_url: urls.map((url) => url.res),
        product_picture_url_publicid: urls.map((url) => url.public_id),
        quantity: quantity,
        sold: false,
        is_publish: is_publish,
        category_id: category_id,
        user_id: req.user.id,
      });
      return res.status(201).json({
        code: 201,
        status: "Sucess",
        result: previewProduct,
      });
    } else if (is_publish === "true") {
      const urls = [];
      const files = req.files;
      if (files.length <= 0) {
        return res.status(403).json({
          code: 403,
          status: "Failed",
          message: "Foto Produk Harus Di Isi",
        });
      }
      for (const file of files) {
        const { path } = file;
        const newPath = await cloudinaryImageUploadMethod(path);
        urls.push(newPath);
      }
      const { name, price, description, quantity, category_id } = req.body;
      const publishProduct = await Product.create({
        name: name,
        price: price,
        description: description,
        product_picture_url: urls.map((url) => url.res),
        product_picture_url_publicid: urls.map((url) => url.public_id),
        quantity: quantity,
        sold: false,
        is_publish: is_publish,
        category_id: category_id,
        user_id: req.user.id,
      });
      const notif = await notification.create({
        user_id: publishProduct.user_id,
        transaction_id: null,
        message: `Product ${publishProduct.name} Rp${publishProduct.price} Berhasil Di Publikasi`,
        product_id: publishProduct.id,
        status: notification.rawAttributes.status.values[1],
      });

      return res.status(201).json({
        code: 201,
        status: "Sucess",
        result: publishProduct,
        message: notif.message,
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(503).json({
      code: 503,
      status: "Error",
      message: error,
    });
  }
};

const publishMyProduct = async (req, res) => {
  try {
    const { id } = req.params;
    const product = await Product.findOne({
      where: {
        id: id,
        user_id: req.user.id,
      },
    });
    if (!product) {
      return res.status(404).json({
        code: 404,
        status: "Failed",
        message: "Product Not Found",
      });
    }
    await Product.update(
      {
        is_publish: true,
      },
      {
        where: {
          id: id,
        },
      }
    );
    const notif = await notification.create({
      user_id: req.user.id,
      product_id: id,
      transaction_id: null,
      message: `Has been published successfully ${product.name} Rp${product.price}`,
      product_id: product.id,
      status: notification.rawAttributes.status.values[1],
    });

    return res.status(201).json({
      code: 201,
      status: "Success",
      message: notif.message,
    });
  } catch (error) {
    console.log(error);
    return res.status(503).json({
      code: 503,
      status: "Error",
      message: error,
    });
  }
};

const putMyProduct = async (req, res) => {
  try {
    const { id } = req.params;
    const { name, price, description, quantity, sold, category_id } = req.body;

    // Delete Picture Product Used Before
    const foundProduct = await Product.findOne({
      where: {
        id: id,
        user_id: req.user.id,
      },
    });

    if (!foundProduct) {
      return res.status(404).json({
        code: 404,
        status: "Failed",
        message: "Product Not Found",
      });
    }

    //options update product
    let options = {
      name: name,
      price: price,
      description: description,
      quantity: quantity,
      sold: sold,
      category_id: category_id,
    };

    //Where Condition for Update Product
    let where = {
      id: id,
      user_id: req.user.id,
    };

    //Upload Image
    const urls = [];
    const files = req.files;
    if (files.length > 0) {
      //Delete Image Before
      for (const file of foundProduct.product_picture_url_publicid) {
        await cloudinaryImageDeleteMethod(file);
      }

      //Upload New Image Product
      for (const file of files) {
        const { path } = file;
        const newPath = await cloudinaryImageUploadMethod(path);
        urls.push(newPath);
      }

      options.product_picture_url = urls.map((url) => url.res);
      options.product_picture_url_publicid = urls.map((url) => url.public_id);
    }

    //Product Update
    const updatedProduct = await Product.update(options, { where });
    if (updatedProduct) {
      return res.status(201).json({
        code: 201,
        status: "Success",
        message: "Product Updated",
        data: updatedProduct,
      });
    }
    return res.status(400).json({
      code: 400,
      status: "Failed",
      message: "Product Not Updated",
    });
  } catch (error) {
    console.log(error);
    return res.status(503).json({
      code: 503,
      status: "Error",
      message: error,
    });
  }
};

const confirmTransaction = async (req, res) => {
  try {
    const { id } = req.params;
    const { confirmation } = req.query;
    const transaction = await Transaction.findOne({
      where: {
        id: id,
      },
    });
    const product = await Product.findOne({
      where: {
        id: transaction.product_id,
      },
    });

    if (!transaction) {
      return res.status(404).json({
        code: 404,
        status: "Failed",
        message: "Transaction Not Found",
      });
    }

    if (confirmation === "true") {
      await Transaction.update(
        {
          confirmation: true,
        },
        {
          where: {
            id: id,
          },
        }
      );
      const notif = await notification.create({
        user_id: transaction.buyer_id,
        product_id: transaction.product_id,
        transaction_id: transaction.id,
        message: `Product ${product.name} Berhasil Ditawar Dengan Harga Rp${transaction.bargain_price}.`,
      });
      return res.status(201).json({
        code: 201,
        status: "Success",
        message: notif.message,
      });
    } else if (confirmation === "false") {
      await Transaction.update(
        {
          confirmation: false,
        },
        {
          where: {
            id: id,
          },
        }
      );
      await transaction.destroy({
        where: {
          id: id,
          confirmation: false,
        },
      });
      // const notif = await notification.create({
      //   user_id: transaction.buyer_id,
      //   transaction_id: transaction.id,
      //   message: `Your Bargain Product ${product.name} Rp${product.price} Has been rejected`,
      // });
      return res.status(201).json({
        code: 201,
        status: "Success",
        message: notif.message,
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(503).json({
      code: 503,
      status: "Error",
      message: error,
    });
  }
};

const changeTransactionStatus = async (req, res) => {
  try {
    const { id } = req.params;
    const { sold } = req.query;
    const product = await Product.findOne({
      where: {
        id: id,
        user_id: req.user.id,
      },
    });

    if (!product) {
      return res.status(404).json({
        code: 404,
        status: "Failed",
        message: "Product Not Found",
      });
    }

    if (sold === "true") {
      await Product.update(
        {
          sold: true,
        },
        {
          where: {
            id: id,
          },
        }
      );

      //membuat notifikasi jika produk terjual
      await notification.create({
        user_id: product.user_id,
        product_id: product.id,
        message: `Product Anda Berhasil Terjual`,
      });
      return res.status(201).json({
        code: 201,
        status: "Success",
        message: "Product Sold",
      });
    } else if (sold === "false") {
      await Product.update(
        {
          sold: false,
        },
        {
          where: {
            id: id,
          },
        }
      );
      await Transaction.destroy({
        where: {
          confirmation: true,
          product_id: id,
        },
      });
      return res.status(201).json({
        code: 201,
        status: "Success",
        message: "Product Not Sold",
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(503).json({
      code: 503,
      status: "Error",
      message: error,
    });
  }
};

const getAllMyProduct = async (req, res) => {
  try {
    let { page, row } = req.query;
    page -= 1;
    const allProduct = await Product.findAll({
      where: {
        user_id: req.user.id,
        sold: false,
      },
      offset: page * row,
      limit: row,
      include: [
        {
          model: Category,
          attributes: ["category"],
        },
      ],
    });
    if (allProduct) {
      return res.status(201).json({
        code: 201,
        status: "Success",
        message: "All Product",
        data: allProduct,
      });
    } else if (allProduct.length === 0) {
      return res.status(404).json({
        code: 404,
        status: "Failed",
        message: "Product Not Found",
      });
    } else {
      return res.status(400).json({
        code: 400,
        status: "Failed",
        message: "Product Not Found",
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(503).json({
      code: 503,
      status: "Error",
      message: error,
    });
  }
};

const getMyProductById = async (req, res) => {
  try {
    const product = await Product.findOne({
      where: {
        id: req.params.id,
        user_id: req.user.id,
        sold: false,
      },
      include: [
        {
          model: Category,
          attributes: ["category"],
        },
      ],
    });
    if (product) {
      return res.status(201).json({
        code: 201,
        status: "Success",
        message: "Product",
        data: product,
      });
    }
    return res.status(400).json({
      code: 400,
      status: "Failed",
      message: "Error Get Product",
    });
  } catch (error) {
    console.log(error);
    return res.status(503).json({
      code: 503,
      status: "Error",
      message: error,
    });
  }
};

const getAllMySoldProduct = async (req, res) => {
  try {
    const { id } = req.user;
    let { page, row } = req.query;
    page -= 1;
    const allProduct = await Product.findAll({
      where: {
        user_id: req.user.id,
        sold: true,
      },
      offset: page * row,
      limit: row,
      include: [
        {
          model: Category,
          attributes: ["category"],
        },
        {
          model: Transaction,
          attributes: ["bargain_price"],
          where: {
            confirmation: true,
            seller_id: req.user.id,
          },
        },
      ],
    });
    if (allProduct) {
      return res.status(200).json({
        code: 200,
        status: "Success",
        message: "All Product",
        data: allProduct,
      });
    } else if (allProduct.length <= 0) {
      return res.status(404).json({
        code: 404,
        status: "Failed",
        message: "Product Not Found",
      });
    } else {
      return res.status(400).json({
        code: 400,
        status: "Failed",
        message: "Product Not Found",
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(503).json({
      code: 503,
      status: "Error",
      message: error,
    });
  }
};

const getMySoldProductById = async (req, res) => {
  try {
    let { page, row } = req.query;
    page -= 1;
    const soldProduct = await Product.findOne({
      where: {
        id: req.params.id,
        user_id: req.user.id,
        sold: true,
      },
      offset: page * row,
      limit: row,
      include: [
        {
          model: Category,
          attributes: ["category"],
        },
      ],
    });
    if (soldProduct) {
      return res.status(201).json({
        code: 201,
        status: "Success",
        message: "Sold Product",
        data: soldProduct,
      });
    }
    return res.status(400).json({
      code: 400,
      status: "Failed",
      message: "Error Get Sold Product",
    });
  } catch (error) {
    console.log(error);
    return res.status(503).json({
      code: 503,
      status: "Error",
      message: error,
    });
  }
};

const getAllMyTransaction = async (req, res) => {
  try {
    let { page, row } = req.query;
    page -= 1;
    const allTransaction = await Transaction.findAll({
      attributes: [
        "id",
        "bargain_price",
        "buyer_id",
        "product_id",
        "confirmation",
        [
          sequelize.fn(
            "to_char",
            sequelize.col("Transaction.createdAt"),
            "DD Mon, HH24:MI:SS"
          ),
          "createdAt",
        ],
      ],
      where: {
        seller_id: req.user.id,
      },
      offset: page * row,
      limit: row,
      include: [
        {
          model: Product,
          attributes: ["id", "name", "product_picture_url", "price", "sold"],
          where: {
            sold: false,
          },
        },
        {
          model: User,
          attributes: ["username"],
        },
      ],
    });

    if (allTransaction) {
      return res.status(200).json({
        code: 200,
        status: "Success",
        message: "All Transaction",
        data: allTransaction,
      });
    }
    return res.status(400).json({
      code: 400,
      status: "Failed",
      message: "Error Get All Transaction",
    });
  } catch (error) {
    console.log(error);
    return res.status(503).json({
      code: 503,
      status: "Error",
      message: error,
    });
  }
};

const getMyTransactionById = async (req, res) => {
  try {
    //Data Transaction
    const transaction = await Transaction.findOne({
      attributes: [
        "id",
        "buyer_id",
        "bargain_price",
        "confirmation",
        [
          sequelize.fn(
            "to_char",
            sequelize.col("Transaction.createdAt"),
            "DD Mon, HH24:MI:SS"
          ),
          "createdAt",
        ],
      ],
      where: {
        id: req.params.id,
        seller_id: req.user.id,
      },
      include: [
        {
          model: Product,
          attributes: ["id", "name", "product_picture_url", "price", "sold"],
        },
        {
          model: User,
          attributes: ["username"],
        },
        {
          model: notification,
          attributes: ["status"],
        },
      ],
    });

    if (!transaction) {
      return res.status(400).json({
        code: 400,
        status: "Failed",
        message: "Error Saat Get Transaksi, Transaksi Tidak Ada",
      });
    }

    //Data Buyer
    const buyer = await User.findOne({
      where: {
        id: transaction.buyer_id,
      },
      attributes: ["id", "username"],
      include: [
        {
          model: Profile,
          attributes: [
            "name",
            "address",
            "phone_number",
            "profile_picture_url",
          ],
          include: {
            model: City,
            attributes: ["name"],
          },
        },
      ],
    });
    //Pengkondisian untuk product sold dll

    //Jika Belom dilakukan Transaksi
    if (transaction.confirmation == null) {
      return res.status(201).json({
        code: 201,
        status: "Success",
        buyer: buyer,
        data: transaction,
      });
    }
    //Ketika Penawaran DIterima dan Sudah Terjual
    else if (
      transaction.confirmation == true &&
      transaction.Product.sold == true
    ) {
      return res.status(200).json({
        code: 200,
        status: "Sucess",
        message: "Transaksi Sudah Berhasil Dilakukan",
        buyer: buyer,
        data: transaction,
      });
    }
    //Ketikan Penawaran Diterima tetapi belom terjual
    else if (transaction.confirmation == true) {
      return res.status(200).json({
        code: 200,
        status: "Sucess",
        message: "Penawaran Diterima",
        buyer: buyer,
        data: transaction,
      });
    }

    //Ketika Penawaran Ditolak Transaksi dihapus
  } catch (error) {
    console.log(error);
    return res.status(503).json({
      code: 503,
      status: "Error",
      message: error,
    });
  }
};

const deleteMyProduct = async (req, res) => {
  try {
    const deletedProduct = await Product.destroy({
      where: {
        id: req.params.id,
        user_id: req.user.id,
      },
    });
    if (deletedProduct) {
      return res.status(201).json({
        code: 201,
        status: "Success",
        message: "Product Deleted",
        data: deletedProduct,
      });
    }
    return res.status(400).json({
      code: 400,
      status: "Failed",
      message: "Product Not Deleted",
    });
  } catch (error) {
    console.log(error);
    return res.status(503).json({
      code: 503,
      status: "Error",
      message: error,
    });
  }
};

module.exports = {
  postMyProduct,
  publishMyProduct,
  putMyProduct,
  confirmTransaction,
  changeTransactionStatus,
  getAllMyProduct,
  getMyProductById,
  getAllMySoldProduct,
  getMySoldProductById,
  getAllMyTransaction,
  getMyTransactionById,
  deleteMyProduct,
};

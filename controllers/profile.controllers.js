const { User, Profile } = require("../models");
const {
  cloudinaryImageDeleteMethod,
  cloudinaryImageUploadMethodProfile,
} = require("../handler/cloudinary");

const profileController = {
  update: async (req, res) => {
    try {
      const { name, address, phone_number, city_id } = req.body;

      //Options For Update
      let options = {
        name: name,
        address: address,
        phone_number: phone_number,
        city_id: city_id,
      };

      //Where Condition
      let where = {
        user_id: req.user.id,
      };

      //Check If Profile Picture Update
      const file = req.file;
      const urls = [];
      if (file) {
        // Delete Picture In Clodinary
        const foundProfile = await Profile.findOne({
          where: {
            user_id: req.user.id,
          },
        });
        if (
          foundProfile.profile_picture_url != process.env.IMAGE_PROFILE_DEFAULT
        ) {
          const deleteImageProfile = await cloudinaryImageDeleteMethod(
            foundProfile.profile_picture_url_publicid
          );
        }

        // Upload New Picture
        const { path } = file;
        const newPath = await cloudinaryImageUploadMethodProfile(path);
        urls.push(newPath);

        options.profile_picture_url = urls[0].res;
        options.profile_picture_url_publicid = urls[0].public_id;
      }

      //Update Data
      const profileUpdated = await Profile.update(options, { where });
      if (profileUpdated) {
        const options = {
          attributes: { exclude: ["createdAt", "updatedAt", "is_publish"] },
          include: {
            model: User,
            attributes: ["username"],
          },
        };
        const foundProfile = await Profile.findOne({
          where: {
            user_id: req.user.id,
          },
          ...options,
        });

        return res.status(201).json({
          code: 201,
          status: "Sucess",
          message: "Profile Updated",
          result: foundProfile,
        });
      }
      return res.status(400).json({
        code: 400,
        status: "Failed",
        message: "Error Updated Profile",
      });
    } catch (error) {
      console.log(error);
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: "Error System",
      });
    }
  },
  getProfile: async (req, res) => {
    try {
      const options = {
        attributes: {
          exclude: ["createdAt", "updatedAt", "profile_picture_url_publicid"],
        },
        include: {
          model: User,
          attributes: ["username"],
        },
      };
      const foundProfile = await Profile.findOne({
        where: {
          user_id: req.user.id,
        },
        ...options,
      });
      if (foundProfile) {
        return res.status(201).json({
          code: 201,
          status: "Sucess",
          message: "Profile Found",
          result: foundProfile,
        });
      }
      return res.status(404).json({
        code: 404,
        status: "Failed",
        message: "Profile Not Found",
      });
    } catch (error) {
      console.log(error);
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: "Error System",
      });
    }
  },
};

module.exports = profileController;

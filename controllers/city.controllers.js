const { City } = require("../models");
const { Op } = require("sequelize");

const cityController = {
  getAllCity: async (req, res) => {
    try {
      const { name } = req.query;
      const options = {
        attributes: ["name"],
        order: [["name", "ASC"]],
      };
      if (name) {
        options.where = {
          name: {
            [Op.iLike]: `%${name}%`,
          },
        };
      }
      const allCity = await City.findAll(options);
      if (allCity.length <= 0) {
        return res.status(404).json({
          code: 404,
          status: "Failed",
          message: "Data City Empty",
        });
      }
      return res.status(201).json({
        code: 201,
        status: "Sucess",
        result: allCity,
      });
    } catch (error) {
      console.log(error);
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: "System Error",
      });
    }
  },
  getCityById: async (req, res) => {
    const cityId = req.params.id;
    try {
      const options = {
        attributes: ["name"],
      };

      const foundCity = await City.findByPk(cityId, options);
      if (!foundCity) {
        return res.status(404).json({
          code: 404,
          status: "Failed",
          message: "City Not Found",
        });
      }
      return res.status(201).json({
        code: 201,
        status: "Sucess",
        result: foundCity,
      });
    } catch (error) {
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: "System Error",
      });
    }
  },
  createCity: async (req, res) => {
    try {
      const { name } = req.body;
      const createdCity = await City.create({
        name: name.toUpperCase(),
      });
      if (!createdCity) {
        return res.status(404).json({
          code: 404,
          status: "Failed",
          message: "City Not Created",
        });
      }
      return res.status(201).json({
        code: 201,
        status: "Sucess",
        message: "Success Created City",
      });
    } catch (error) {
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: "System Error",
      });
    }
  },
  deleteCity: async (req, res) => {
    try {
      const { name } = req.body;
      const deletedCity = await City.destroy({
        where: {
          name: name,
        },
      });
      if (!deletedCity) {
        return res.status(404).json({
          code: 404,
          staus: "Failed",
          message: "City Not Found",
        });
      }
      return res.status(201).json({
        code: 201,
        status: "Sucess",
        message: "City Deleted",
      });
    } catch (error) {
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: "System Error",
      });
    }
  },
};

module.exports = cityController;

const { Category } = require("../models");

const categoryController = {
  getAllCategory: async (req, res) => {
    try {
      const options = {
        attributes: ["category", "id"],
        order: [["id", "ASC"]],
      };
      const allCategory = await Category.findAll(options);
      if (allCategory.length <= 0) {
        return res.status(404).json({
          code: 404,
          status: "Failed",
          message: "Empty Data",
        });
      }
      return res.status(200).json({
        code: 200,
        status: "success",
        result: allCategory,
      });
    } catch (error) {
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: "System Error",
      });
    }
  },
  getCategoryById: async (req, res) => {
    const categoryId = req.params.id;
    try {
      const options = {
        attributes: ["category", "id"],
      };
      const foundCategory = await Category.findByPk(categoryId, options);
      if (!foundCategory) {
        return res.status(404).json({
          code: 404,
          status: "Failed",
          message: "Category Not Found",
        });
      }
      return res.status(200).json({
        code: 200,
        status: "Sucess",
        result: foundCategory,
      });
    } catch (error) {
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: "System Error",
      });
    }
  },
};

module.exports = categoryController;

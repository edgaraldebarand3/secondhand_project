const {
  User,
  Profile,
  Product,
  Transaction,
  notification,
  Category,
  City,
} = require("../models");
const { Op } = require("sequelize");

const transactionControllers = {
  getAllTransaction: async (req, res) => {
    try {
      const options = {
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
        include: [
          {
            model: Product,
            attributes: {
              exclude: ["createdAt", "updatedAt", "is_publish"],
            },
          },
        ],
      };

      const getAllTransaction = await Transaction.findAll(options);
      console.log(getAllTransaction);
      return res.status(200).json({
        code: 200,
        status: "Sucess",
        result: getAllTransaction,
      });
    } catch (error) {
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: error,
      });
    }
  },
  createTransaction: async (req, res) => {
    try {
      const { bargain_price, product_id } = req.body;

      const foundProduct = await Product.findByPk(product_id);

      if (!foundProduct) {
        return res.status(404).json({
          code: 404,
          status: "Failed",
          message: "Product Not Found",
        });
      }

      if (foundProduct.user_id == req.user.id) {
        return res.status(401).json({
          code: 401,
          status: "Failed",
          message: "Can't Bid Your Own Product",
        });
      }

      //Cek Apa Sudah Pernah Menawar Sebelumnya
      const foundTransaction = await Transaction.findAll({
        where: {
          buyer_id: req.user.id,
          seller_id: foundProduct.user_id,
          product_id: product_id,
          confirmation: null,
        },
      });
      console.log(foundTransaction.length);
      if (foundTransaction.length > 0) {
        return res.status(403).json({
          code: 403,
          status: "Failed",
          message: "Penawaran Sedang Di Proses",
        });
      }
      const createTransaction = await Transaction.create({
        bargain_price: bargain_price,
        buyer_id: req.user.id,
        seller_id: foundProduct.user_id,
        product_id: foundProduct.id,
      });

      if (!createTransaction) {
        return res.status(400).json({
          code: 400,
          status: "Failed",
          message: "Transaction Failed",
        });
      }

      //Produk ditawar Notif untuk Buyer
      await notification.create({
        product_id: foundProduct.id,
        transaction_id: createTransaction.id,
        user_id: createTransaction.seller_id,
        message: `Produk Ditawar Harga Rp.${bargain_price}`,
        status: notification.rawAttributes.status.values[0],
      });

      //Notif Untuk Seller
      await notification.create({
        product_id: foundProduct.id,
        transaction_id: createTransaction.id,
        user_id: req.user.id,
        message: `Anda Menawar Produk Dengan Harga Rp.${bargain_price}`,
        status: notification.rawAttributes.status.values[0],
      });

      return res.status(201).json({
        code: 201,
        status: "Sucess",
        result: createTransaction,
      });
    } catch (error) {
      console.log(error);
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: "System Error",
      });
    }
  },

  getTransactionByProductId: async (req, res) => {
    try {
      const productId = req.params.id;
      //Data Transaction
      const transaction = await Transaction.findOne({
        attributes: [
          "id",
          "buyer_id",
          "bargain_price",
          "confirmation",
          "product_id",
        ],
        where: {
          product_id: productId,
          seller_id: req.user.id,
          confirmation: true,
        },
        include: [
          {
            model: Product,
            attributes: ["id", "name", "product_picture_url", "price", "sold"],
          },
          {
            model: User,
            attributes: ["username"],
          },
          {
            model: notification,
            attributes: ["status"],
          },
        ],
      });

      //Data Buyer
      const buyer = await User.findOne({
        where: {
          id: transaction.buyer_id,
        },
        attributes: ["id", "username"],
        include: [
          {
            model: Profile,
            attributes: [
              "name",
              "address",
              "phone_number",
              "profile_picture_url",
            ],
            include: {
              model: City,
              attributes: ["name"],
            },
          },
        ],
      });

      //If Transaction exist
      if (transaction.Product.sold == true) {
        return res.status(200).json({
          code: 200,
          status: "Sucess",
          dataTransaction: transaction,
          dataBuyer: buyer,
        });
      }

      return res.status(404).json({
        code: 404,
        status: "Failed",
        message: "Transaction Not Found",
      });
    } catch (error) {
      console.log(error);
      return res.status(503).json({
        code: 503,
        status: "error",
        message: "System Error",
      });
    }
  },
};

module.exports = transactionControllers;

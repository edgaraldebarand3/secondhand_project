const { User } = require("../models");
const { sendMail } = require("../handler/mailjet");
const bcrypt = require("bcrypt");

const getOTP = async (req, res) => {
  const { email } = req.body;
  try {
    const foundUser = await User.findOne({
      where: {
        email: email,
      },
    });
    console.log(foundUser);
    if (foundUser) {
      const otp = Math.floor(206265 + Math.random() * 999999).toString();
      const hashedOTP = bcrypt.hashSync(otp, 10);
      await User.update(
        {
          otp: hashedOTP,
        },
        {
          where: {
            email: email,
          },
        }
      );
      sendMail(email, foundUser.username, hashedOTP);
      return res.status(201).json({
        code: 201,
        status: "Success",
        message: "OTP Sent",
      });
    }
    return res.status(404).json({
      code: 404,
      status: "Failed",
      message: "Email Not Found",
    });
  } catch (error) {
    return res.status(503).json({
      code: 503,
      status: "Failed",
      message: error.message,
    });
  }
};

const resetPassword = async (req, res) => {
  const { reset } = req.query;
  const { new_password, repeat_pass } = req.body;
  if (!reset) {
    return res.status(400).json({
      code: 400,
      status: "Failed",
      message: "Reset link needed",
    });
  }
  if (!new_password || !repeat_pass) {
    return res.status(400).json({
      code: 400,
      status: "Failed",
      message: "New password and repeat password needed",
    });
  }
  if (new_password !== repeat_pass) {
    return res.status(400).json({
      code: 400,
      status: "Failed",
      message: "New password and repeat password must match",
    });
  }

  await User.findOne({
    where: {
      otp: reset,
    },
  })
    .then((user) => {
      if (!user) {
        return res.status(404).json({
          code: 404,
          status: "Failed",
          message: "User not found / Email not registered",
        });
      }
      user
        .update({
          password: bcrypt.hashSync(new_password, 10),
          otp: null,
        })
        .then(() => {
          return res.status(201).json({
            code: 201,
            status: "Success",
            message: "Password updated",
          });
        })
        .catch((err) => {
          return res.status(503).json({
            code: 503,
            status: "Failed",
            message: "Error while updating password",
          });
        });
    })
    .catch((err) => {
      return res.status(503).json({
        code: 503,
        status: "Failed",
        message: "Error while checking email",
      });
    });
};

module.exports = {
  getOTP,
  resetPassword,
};

const { User, Profile } = require("../models");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { SALT_ROUNDS, JWT_SECRET } = process.env;
const { registerMail } = require("../handler/mailjet");

const userController = {
  login: async (req, res) => {
    const { email, password } = req.body;
    const foundUser = await User.findOne({
      where: {
        email: email,
      },
    });

    if (foundUser) {
      const isValidPassword = bcrypt.compareSync(password, foundUser.password);
      if (isValidPassword) {
        const payload = {
          id: foundUser.id,
          username: foundUser.username,
          email: foundUser.email,
        };
        const token = jwt.sign(payload, JWT_SECRET, {
          expiresIn: "7d",
        });

        const foundProfile = await Profile.findOne({
          attributes: [
            "id",
            "name",
            "city_id",
            "profile_picture_url",
            "phone_number",
            "address",
          ],
          where: {
            user_id: foundUser.id,
          },
        });
        return res.status(201).json({
          token: token,
          profileData: foundProfile,
        });
      }
      return res.status(400).json({
        code: 400,
        status: "Failed",
        message: "Wrong email or password",
      });
    }
    return res.status(404).json({
      code: 404,
      status: "Failed",
      message: "Email Not Registered",
    });
  },
  register: async (req, res) => {
    const { username, email, password } = req.body;
    try {
      const foundUser = await User.findOne({
        where: {
          email: email,
        },
      });
      if (!foundUser) {
        const registerUser = await User.create({
          username: username,
          email: email,
          password: password,
        });
        const createProfile = await Profile.create({
          name: registerUser.username,
          user_id: registerUser.id,
          profile_picture_url: process.env.IMAGE_PROFILE_DEFAULT,
          profile_picture_url_publicid:
            process.env.IMAGE_PROFILE_PUBLICID_DEFAULT,
        });
        registerMail(email, username);
        return res.status(201).json({
          code: 201,
          status: "Success",
          message: "Registration Sucess",
        });
      }
      //if user found return error
      return res.status(400).json({
        code: 400,
        status: "Failed",
        message: "Email Already In Use",
      });
    } catch (error) {
      console.log(error);
      return res.status(503).json({
        code: 503,
        status: "Failed",
        message: "System Error",
      });
    }
  },
  getAll: async (req, res) => {
    try {
      let { page, row } = req.query;

      page -= 1;

      const options = {
        attributes: {
          exclude: ["createdAt", "updatedAt", "password"],
        },
        offset: page * row,
        limit: row,
        order: [["id", "ASC"]],
      };

      const allUser = await User.findAll(options);
      if (allUser.length <= 0) {
        return res.status(404).json({
          code: 404,
          status: "Failed",
          message: "Empty Data",
        });
      }
      return res.status(201).json({
        code: 201,
        status: "success",
        result: allUser,
      });
    } catch (error) {
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: "System Error",
      });
    }
  },
  create: async (req, res) => {
    try {
      const { username, email, password } = req.body;
      const createdUser = User.create({
        username: username,
        email: email,
        password: password,
      });
      if (createdUser) {
        const createProfile = await Profile.create({
          name: createdUser.username,
          user_id: createdUser.id,
        });

        return res.status(201).json({
          code: 201,
          status: "Sucess",
          message: `User created with email ${email}`,
        });
      }
      return res.status(400).json({
        code: 400,
        status: "Failed",
        message: "User Not Created",
      });
    } catch (error) {
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: "System Error",
      });
    }
  },
  update: async (req, res) => {
    const userId = req.params.id;
    try {
      const { username, email, password } = req.body;
      const updatedUser = User.update(
        {
          username: username,
          email: email,
          password: password,
        },
        {
          where: {
            id: userId,
          },
        }
      );
      if (updatedUser) {
        return res.status(201).json({
          code: 201,
          status: "Sucess",
          message: "User Updated",
        });
      }
      return res.status(400).json({
        code: 400,
        status: "Failed",
        message: "User not Update",
      });
    } catch (error) {
      return res.status(503).json({
        code: 503,
        status: "Error",
        message: "System Error",
      });
    }
  },
};

module.exports = userController;
